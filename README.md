Transformer that will replace string in file.

## Usage

Simply add the following lines to your `pubspec.yaml`:

    :::yaml
    dependencies:
      replace_string_in_file: any
    transformers:
      - replace_string_in_file

## Configuration

You can specify patterns:

    :::yaml
    transformers:
      - replace_string_in_file:
          pattern: 
          - /path/to/file_1;;old_string;;new_string;;old_string_n;;new_string_n
          - /path/to/file_n;;old_string;;new_string;;old_string_n;;new_string_n
