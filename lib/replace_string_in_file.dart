library replace_string_in_file.transformer;

import 'dart:async';
import 'package:barback/barback.dart';

/// Transformer used by `pub build` to replace string in file.
class ReplaceStringTransformer extends Transformer {
  final BarbackSettings settings;
  final TransformerOptions options;

  Transformer tr;
  
  ReplaceStringTransformer(BarbackSettings settings) :
    settings = settings,
    options = new TransformerOptions.parse(settings.configuration);

  ReplaceStringTransformer.asPlugin(BarbackSettings settings) :
    this(settings);

  Future<bool> isPrimary(AssetId input) {
    bool primary = false;
    if (options.patterns != null) {
      for (String pattern in options.patterns) {
        List<String> arr = pattern.split(';;');
        if (arr[0].contains(input.path)) {
          primary = true;
          break;
        }
      }
    }
    return new Future.value(primary);
  }

  Future apply(Transform transform) {
    AssetId primaryAssetId = transform.primaryInput.id;
    print(primaryAssetId.path);
    for (String pattern in options.patterns) {
      List<String> arr = pattern.split(';;');
      if (arr[0].contains(transform.primaryInput.id.path)) {
        if (arr.length > 0) {
          arr.removeAt(0);
        }
        return transform.primaryInput.readAsString().then((content) {
          if (content is String) {
            String newContent = content;
            int i = 0;
            while (i < arr.length) {
              String oldString = arr[i];
              String newString = arr[i+1];
              newContent = newContent.replaceAll(oldString,  newString);
              print('Replaced "' + oldString + '" by "' + newString + '" in ' + primaryAssetId.path);
              i = i + 2;
            }
            transform.addOutput(new Asset.fromString(primaryAssetId, newContent));
          }
        });
      }
    }
    return null;
  }
}

class TransformerOptions {
  final List<String> patterns;

  TransformerOptions({List<String> this.patterns});

  factory TransformerOptions.parse(Map configuration) {
    config(key, defaultValue) {
      var value = configuration[key];
      return value != null ? value : defaultValue;
    }

    return new TransformerOptions(
        patterns: config("patterns", []));
  }
}
